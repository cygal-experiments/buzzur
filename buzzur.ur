table entry : { Id: int, Title: string, Created : time, Author : string, Link : string }
  PRIMARY KEY Id

style link

fun list () =
   rows <- queryX (SELECT * FROM entry)
      (fn row => 
          <xml>
            <li class={link}>{[row.Entry.Author]} posted {[row.Entry.Title]} ({[row.Entry.Link]})</li>
          </xml>
      );
    return 
      <xml>
        <head>
         <title>All Entries</title>
         <link type="text/css" rel="stylesheet" href="http://awesom.eu/~cygal/style.css" />
        </head>
        <body>
          <h1>All Entries</h1>
          <ul>
          {rows}
          </ul>
        </body>
      </xml>
